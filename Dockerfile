FROM nginx:alpine

RUN rm /etc/nginx/conf.d/default.conf

COPY watchmen/* /etc/nginx/conf.d/

